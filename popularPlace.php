<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="master.css">
    <link rel="stylesheet" href="content.css">
    <title>Popular Place</title>
  </head>
  <body>
    <div class="logo">
      <h2>Travel</h2>
    </div>
    <nav class="main-nav">
    <ul class="menu">
    <li class="close"></li>
    <li><a href="index.html">home</a></li>
    <li><a href="">photo stories</a></li>
    <li><a href="about-us.html">about us</a></li>
    <li><a href="contact.html">contact</a></li>
    <a href="register.html"><button id="logIn">Login</button></a>
  </ul>
    <span class="mobile-btn">☰</span>
    <div class="overlay"></div>
        </nav>
        <div class="content">
        <h1 class="entry-title">Brezovica</h1>
        <div class="entry-content">
          <strong>Brezovica is a popular ski resort in the south of Kosovo,
            close to the border with Macedonia. The place is situated in the
            beautiful Sharr Mountains.</strong>
            <h2>Location of Brezovica</h2>
            <p>The Brezovica area is a 1,5 hour drive from the Pristina
               international airport, and close to the airport of neighboring
               country Macedonia.
                The closest town is Shtërpcë (in Serbian: Štrpce).</p>
            <h2>Skiing and snowboarding</h2>
             <p>There are nine ski runs located in the area, with distances
                between 300 meters and 3.500 meters. The top of the biggest
                hill is around 2.500 meters (8200 feet) high. An online graph
                shows the amount of snow during five winters (scroll down and
                click on the graph). Due to a big amount of snow, it
                is a popular spot for backcountry (off-piste) skiing and snowboarding.</p>
              <h2>Foreign investment in Brezovica</h2>
              <img class="alignright size-full wp-image-922"
              src="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-skiing.jpg"
              alt="brezovica-ski-resort" width="400" height="300"
              srcset="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-skiing.jpg 400w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-skiing-300x225.jpg 300w"
              sizes="(max-width: 400px) 100vw, 400px">
              <p>In the spring of 2015, French companies signed a deal to
                 invest 400 million euros in the ski resort. According to Kosovar
                 officials, the future wintersport accommodation will create
                 thousands of jobs. Brezovica will offer 128 ski-able days a year,
                 and will offer recreation in the summertime. The New York Times wrote a
                 long article about this. In the summer of 2016, media reported that
                 the future of the investment was unclear because of financial reasons.</p>
             <h2>Hotels in Brezovica</h2>
             <p>There are a couple of hotels in the wintersport area,
                that can accommodate around 700 people. Well known hotels
                 are the Hoteli i Shkollës së skijimit, Molika Hotel and Narcis.
                  The only hotel with a clear website is the Woodland Hotel.</p>
              <h2>Pictures of Brezovica</h2>
              <img class="alignright wp-image-911 size-large"
                  src="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-b-1024x683.jpg"
                  alt="brezovica-ski-mountain" width="665" height="444"
                  srcset="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-b-1024x683.jpg 1024w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-b-300x200.jpg 300w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-b-660x440.jpg 660w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-b.jpg 1296w" sizes="(max-width: 665px) 100vw, 665px">
              <p>Are you curious about great pictures of the ski resort in
                 Brezovica? We offer you the best ones. Thanks to Snownjeri,
                  dozens of beautiful pictures are online available.
                   We show three of them. Note: they are taken in March
                    and April (© Snownjeri). Interested in more? Check for
                     example these two albums of Snownjeri.</p>
           <img class="alignright wp-image-910 size-large"
               src="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-a-1024x683.jpg" alt="brezovica-ski-nature" width="665"
               height="444" srcset="https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-a-1024x683.jpg 1024w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-a-300x200.jpg 300w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-a-660x440.jpg 660w, https://www.kosovo-info.com/wp-content/uploads/2015/09/Brezovica-snow-picture-by-Snownjeri-April-a.jpg 1296w" sizes="(max-width: 665px) 100vw, 665px">
        </div>
      </div>
      <div class="Copyright">
        <p class="travel_copyright">Copyright &copy; 2019, Travel All Rights Reserved</p>
      </div>
        <script type="text/javascript">
          var mobileBtn = document.querySelector('.mobile-btn');
          var menu = document.querySelector('.menu');
          var closeBtn = document.querySelector('.close');
          var overlay = document.querySelector('.overlay');
        // When the user clicks on the hamburger icon the 'open' class is added
        // to both the menu and overlay elements making them visible and triggering the animation
          mobileBtn.addEventListener('click', function(){
            menu.className += ' open';
            overlay.className += ' open';
        })
        // When the close button is clicked the 'open' class is removed from
        // both the menu and overlay elements making them invisible
          closeBtn.addEventListener('click', function(){
            menu.className = 'menu';
            overlay.className = 'overlay';
        })
        // If a user clicks outside the menu on the overlay it will also trigger the event
        // to hide the menu and overlay elements
          window.addEventListener('click', function(event){
            if(event.target === overlay){
              menu.className = 'menu';
              overlay.className = 'overlay';
            }
          })
    </script>
  </body>
</html>
