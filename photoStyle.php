<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="photoStyle.css">
    <link rel="stylesheet" href="styleTravel.css">
    <title>Photo</title>
  </head>
  <body>
    <div class="logo">
      <h2>Travel</h2>
    </div>
    <nav class="main-nav">
    <ul class="menu">
    <li class="close"></li>
    <li><a href="index.html">home</a></li>
    <li><a href="photoStyle.html">photo stories</a></li>
    <li><a href="about-us.html">about us</a></li>
    <li><a href="contact.html">contact</a></li>
    <a href="register.html"><button id="logIn">Login</button></a>
  </ul>
    <span class="mobile-btn">☰</span>
    <div class="overlay"></div>
        </nav>
    <div class="grid-section">
      <ul class="grid">
        <li>
          <div class="xop-box box-img-1">
            <a href="#">
              <h3>Brezovica</h3>
              <p>Lorem ipsum dolor sit amet.</p>
            </a>
          </div>
        </li>
        <li>
          <div class="xop-box box-img-2">
            <a href="#">
              <h3>Zürich</h3>
              <p>Lorem ipsum dolor sit amet.</p>
            </a>
          </div>
        </li>
        <li>
          <div class="xop-box box-img-3">
            <a href="#">
              <h3>Moraine Lake</h3>
              <p>Lorem ipsum dolor sit amet.</p>
            </a>
          </div>
        </li>
        <li>
          <div class="xop-box box-img-4">
            <a href="#">
              <h3>Manarola</h3>
              <p>Lorem ipsum dolor sit amet.</p>
            </a>
          </div>
        </li>
        <li>
          <div class="xop-box box-img-5">
            <a href="#">
              <h3>İstanbul</h3>
              <p>Lorem ipsum dolor sit amet.</p>
            </a>
          </div>
        </li>
      </ul>
    </div>
    <div class="Copyright">
      <p class="travel_copyright">Copyright &copy; 2019, Travel All Rights Reserved</p>
    </div>
    <script type="text/javascript">
    var mobileBtn = document.querySelector('.mobile-btn');
    var menu = document.querySelector('.menu');
    var closeBtn = document.querySelector('.close');
    var overlay = document.querySelector('.overlay');
  // When the user clicks on the hamburger icon the 'open' class is added
  // to both the menu and overlay elements making them visible and triggering the animation
    mobileBtn.addEventListener('click', function(){
      menu.className += ' open';
      overlay.className += ' open';
  })
  // When the close button is clicked the 'open' class is removed from
  // both the menu and overlay elements making them invisible
    closeBtn.addEventListener('click', function(){
      menu.className = 'menu';
      overlay.className = 'overlay';
  })
  // If a user clicks outside the menu on the overlay it will also trigger the event
  // to hide the menu and overlay elements
    window.addEventListener('click', function(event){
      if(event.target === overlay){
        menu.className = 'menu';
        overlay.className = 'overlay';
      }
    })
    </script>
  </body>
</html>
